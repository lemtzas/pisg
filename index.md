---
layout: page
title: "IRC Logs"
description: "Hey there."
#image:
  #feature: texture-feature-02.jpg
  #credit: Texture Lovers
  #creditlink: http://texturelovers.com
---

## Freenode

### [#reddit-gamedev](reddit-gamedev.html)

### [#1GAM](1gam.html)

### [##gamedev](gamedev.html)

### [#boardgames](boardgames.html)

## Snoonet

### [#DnD](dnd.html)